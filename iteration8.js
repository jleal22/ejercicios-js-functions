// Iteration #8: Contador de repeticiones

// Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  Puedes usar este array para probar tu función:

const counterWords = [
    'code',
    'repeat',
    'eat',
    'sleep',
    'code',
    'enjoy',
    'sleep',
    'code',
    'enjoy',
    'upgrade',
    'code'
  ];

  function repeatCounter(param) {
    result = [];
    param.sort();
    previous = param[0];
    count = 1;

    for (let i=1; i <= param.length; i++) {
  
        if ((i == param.length) || (param[i] != previous)) {
            result.push( { "word" : previous, "count" : count} );
            previous = param[i];
            count = 1;
        } else {
            count += 1;
        }
    };

    return result;
  }

  console.log(repeatCounter(counterWords));